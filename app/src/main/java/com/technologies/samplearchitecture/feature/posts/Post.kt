package com.technologies.samplearchitecture.feature.posts

data class Post(
    val id: String,
    val image: String,
    val likes: Int,
    val link: String,
    val owner: Owner,
    val publishDate: String,
    val tags: List<String>,
    val text: String
)

data class Owner(
    val email: String,
    val firstName: String,
    val id: String,
    val lastName: String,
    val picture: String,
    val title: String
)