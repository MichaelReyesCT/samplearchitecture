package com.technologies.samplearchitecture.feature.posts

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

internal interface PostsApi {
    companion object {
        private const val PARAM_POST_ID = "postId"
        private const val POSTS = "post?limit=10"
        private const val POST_DETAILS = "post/{$PARAM_POST_ID}"
    }

    @GET(POSTS) fun posts(): Call<PostResponse?>
    @GET(POST_DETAILS) fun postDetails(@Path(PARAM_POST_ID) postId: String): Call<Post?>
}
