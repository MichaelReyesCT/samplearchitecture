package com.technologies.samplearchitecture.feature.posts

import com.technologies.samplearchitecture.core.di.scope.AppScope
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

@AppScope
class PostsService
@Inject constructor(retrofit: Retrofit) : PostsApi {
    private val postApi by lazy { retrofit.create(PostsApi::class.java) }

    override fun posts() = postApi.posts()
    override fun postDetails(postId: String) = postApi.postDetails(postId)
}
