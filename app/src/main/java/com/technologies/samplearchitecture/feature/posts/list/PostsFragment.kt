package com.technologies.samplearchitecture.feature.posts.list

import android.os.Bundle
import com.technologies.samplearchitecture.R
import com.technologies.samplearchitecture.core.base.BaseActivity
import com.technologies.samplearchitecture.core.base.BaseFragment
import com.technologies.samplearchitecture.core.exception.Failure
import com.technologies.samplearchitecture.core.extensions.observe
import com.technologies.samplearchitecture.core.extensions.viewModel
import com.technologies.samplearchitecture.databinding.FragmentPostsBinding
import kotlinx.android.synthetic.main.fragment_posts.*
import javax.inject.Inject

class PostsFragment : BaseFragment<FragmentPostsBinding>() {

    override val layoutRes = R.layout.fragment_posts

    @Inject
    lateinit var adapter: PostsAdapter

    @Inject
    lateinit var postsViewModel: PostsViewModel

    override fun onCreated(savedInstance: Bundle?) {

        (activity as? BaseActivity<*>)?.setToolbar(show = true, showBackButton = false, title = "Posts")

        initViews()
        initObserver()
    }

    private fun initViews() {
        posts_rv_data.adapter = adapter

        posts_srl.setOnRefreshListener {
            posts_srl.isRefreshing = false
            postsViewModel.fetchPosts()
        }
    }

    private fun initObserver() {
        postsViewModel = viewModel(viewModelFactory){
            fetchPosts()
            observe(posts){ it?.apply { adapter.collection = this } }
            observe(loading){ it?.apply { (activity as? BaseActivity<*>)?.showLoading(this) } }
            observe(failure, ::handleFailure)
        }
    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> showMessage("No Network Error", false)
            is Failure.ServerError -> showMessage("Server Error", false)
        }
    }
}