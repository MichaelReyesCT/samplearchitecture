package com.technologies.samplearchitecture.feature.posts

data class PostResponse(
    val data: List<Post>
)