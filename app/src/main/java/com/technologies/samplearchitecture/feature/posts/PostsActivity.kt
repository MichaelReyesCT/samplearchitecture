package com.technologies.samplearchitecture.feature.posts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.technologies.samplearchitecture.R
import com.technologies.samplearchitecture.core.base.BaseActivity
import com.technologies.samplearchitecture.databinding.ActivityPostsBinding
import kotlinx.android.synthetic.main.activity_posts.*

class PostsActivity : BaseActivity<ActivityPostsBinding>() {
    override val layoutRes = R.layout.activity_posts

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
    }

}