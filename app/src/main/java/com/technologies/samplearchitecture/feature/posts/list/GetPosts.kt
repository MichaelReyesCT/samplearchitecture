package com.technologies.samplearchitecture.feature.posts.list

import com.technologies.samplearchitecture.core.interactor.UseCase
import com.technologies.samplearchitecture.feature.posts.Post
import com.technologies.samplearchitecture.feature.posts.PostsRepository
import javax.inject.Inject

class GetPosts
@Inject constructor(private val postsRepository: PostsRepository) : UseCase<List<Post>, UseCase.None>() {

    override suspend fun run(params: None) = postsRepository.posts()
}
