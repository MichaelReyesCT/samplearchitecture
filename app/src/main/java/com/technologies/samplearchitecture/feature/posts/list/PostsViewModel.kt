package com.technologies.samplearchitecture.feature.posts.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.technologies.samplearchitecture.core.base.BaseViewModel
import com.technologies.samplearchitecture.core.interactor.UseCase
import com.technologies.samplearchitecture.feature.posts.Post
import javax.inject.Inject

class PostsViewModel
@Inject constructor(private val getPosts: GetPosts): BaseViewModel() {

    private val _posts: MutableLiveData<List<Post>> = MutableLiveData()
    val posts: LiveData<List<Post>> = _posts

    fun fetchPosts(){
        setLoading(true)
        getPosts(UseCase.None()) { it.fold(::handleFailure, ::handlePostList) }

    }

    private fun handlePostList(posts: List<Post>) {
        setLoading(false)
        _posts.value = posts
    }
}