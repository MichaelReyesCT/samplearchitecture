package com.technologies.samplearchitecture.feature.posts.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.technologies.samplearchitecture.R
import com.technologies.samplearchitecture.core.utils.AutoUpdatableAdapter
import com.technologies.samplearchitecture.databinding.ItemPostBinding
import com.technologies.samplearchitecture.feature.posts.Post
import javax.inject.Inject
import kotlin.properties.Delegates

class PostsAdapter @Inject constructor(val context: Context) :
    RecyclerView.Adapter<PostsAdapter.Holder>(),
    AutoUpdatableAdapter {

    internal var collection: List<Post> by Delegates.observable(emptyList()) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }


    internal var clickListener: (Post) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder.from(
            parent,
            R.layout.item_post
        )

    override fun getItemCount() = collection.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.binding.apply{
            item = collection[position]
            executePendingBindings()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class Holder(val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup, layout: Int): Holder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = DataBindingUtil.inflate<ItemPostBinding>(inflater, layout, parent, false)
                return Holder(
                    binding
                )
            }
        }
    }
}
