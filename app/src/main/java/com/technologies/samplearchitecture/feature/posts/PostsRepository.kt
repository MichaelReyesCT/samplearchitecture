package com.technologies.samplearchitecture.feature.posts

import com.technologies.samplearchitecture.core.exception.Failure
import com.technologies.samplearchitecture.core.extensions.request
import com.technologies.samplearchitecture.core.functional.Either
import com.technologies.samplearchitecture.core.network.NetworkHandler
import javax.inject.Inject

interface PostsRepository {
    fun posts(): Either<Failure, List<Post>>
    fun postDetails(postId: String): Either<Failure, Post?>

    class Network
    @Inject constructor(private val networkHandler: NetworkHandler,
                        private val service: PostsService) : PostsRepository {

        override fun posts(): Either<Failure, List<Post>> {
            return when (networkHandler.isConnected) {
                true -> request(service.posts(), { it?.data?: emptyList() }, null)
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }

        override fun postDetails(postId: String): Either<Failure, Post?> {
            return when (networkHandler.isConnected) {
                true -> request(service.postDetails(postId), { it }, null)
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }
    }
}