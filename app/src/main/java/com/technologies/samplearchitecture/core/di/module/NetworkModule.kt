package com.technologies.samplearchitecture.core.di.module

import androidx.annotation.NonNull
import com.technologies.samplearchitecture.BuildConfig
import com.technologies.samplearchitecture.core.di.scope.AppScope
import com.technologies.samplearchitecture.feature.posts.PostsRepository

import java.util.concurrent.TimeUnit

import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val BACKEND_BASE_URL = "https://dummyapi.io/data/api/"

@Module
class NetworkModule {

    internal val loggingInterceptor: HttpLoggingInterceptor
        @Provides
        get() = HttpLoggingInterceptor().setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        )

    @Provides
    fun getBackendApiEndpoint(): String {
        return BACKEND_BASE_URL
    }
    @Provides
    @AppScope
    internal fun getHttpClient(
        interceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder
            .addInterceptor(interceptor)
            .addInterceptor(httpApiInterceptor())
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)

        return builder.build()
    }

    @Provides
    @AppScope
    internal fun provideApiRetrofit(
        @NonNull baseUrl: String, client: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    private fun httpApiInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()
            request = request.newBuilder()
                .header("Content-Type", "application/json")
                .header("app-id",BuildConfig.APP_ID)
                .build()
            chain.proceed(request)
        }
    }

    @Provides
    @AppScope
    fun providePostsRepository(dataSource: PostsRepository.Network): PostsRepository = dataSource

}

