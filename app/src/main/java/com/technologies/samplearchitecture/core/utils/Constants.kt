
package com.technologies.samplearchitecture.core.utils

class Constants {
    companion object{
        const val PREF_KEY_NETWORK_CONNECTED = "_pref_network_connected"
    }
}