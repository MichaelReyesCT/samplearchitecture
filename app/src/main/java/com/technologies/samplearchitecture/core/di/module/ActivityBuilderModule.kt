package com.technologies.samplearchitecture.core.di.module

import com.technologies.samplearchitecture.core.di.scope.ActivityScope
import com.technologies.samplearchitecture.feature.posts.PostsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindPostsActivity(): PostsActivity

}