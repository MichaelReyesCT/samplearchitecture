package com.technologies.samplearchitecture.core.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.technologies.samplearchitecture.feature.posts.list.PostsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * For binding viewModels using dagger @Binds
 */
@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PostsViewModel::class)
    abstract fun bindsPostViewModel(viewModel: PostsViewModel): ViewModel
}
