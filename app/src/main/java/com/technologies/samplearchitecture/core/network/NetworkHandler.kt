package com.technologies.samplearchitecture.core.network

import android.content.Context
import com.technologies.samplearchitecture.core.di.scope.AppScope
import com.technologies.samplearchitecture.core.extensions.networkInfo
import javax.inject.Inject

/**
 * Injectable class which returns information about the network connection state.
 */
@AppScope
class NetworkHandler
@Inject constructor(private val context: Context) {
    val isConnected get() = context.networkInfo?.isConnectedOrConnecting
}