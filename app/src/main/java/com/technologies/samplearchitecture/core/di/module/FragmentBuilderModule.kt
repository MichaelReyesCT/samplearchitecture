package com.technologies.samplearchitecture.core.di.module

import com.technologies.samplearchitecture.core.di.scope.FragmentScope
import com.technologies.samplearchitecture.feature.posts.list.PostsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindPostsFragment(): PostsFragment

}